const express = require("express")
const app=express();
const userModel = require("../db/userShema");
const jwt = require('jsonwebtoken')
const secretkey=require("../config")


const login = async function(req, res){


  userModel.findOne({ username: req.body.username })
  .then(data => {
    const userfound=data;
    if(!userfound){
      console.log("err")
      res.status(400).send("err")
    }
    else{
      
    if(userfound.username==req.body.username && userfound.password==req.body.pwd){
      var token =jwt.sign({utilisateur:userfound},secretkey.secret, {
        expiresIn: 86400 
      });
      res.setHeader('x-access-token', 'Bearer '+ token);
      console.log("id"+{id:userfound._id})
      res.status(200).send({ auth: true, token: token ,id:userfound._id});
      console.log({ auth: true, token: token ,id:userfound._id});
    }else{
      res.status(401).send("err")
    }
  }
  })

}


const createUser = async function(req, res){

  try{
  const list=await userModel.find({})
  const bool=verifyUser(list,req.body.username)

  if(!bool){
    res.status(400).send("user exists")
  }
else{
  user = new userModel({
    username: req.body.username.trim().toLowerCase(),
    password: req.body.pwd,
    })

    await user.save();
    var token =jwt.sign({utilisateur:user},secretkey.secret, {
      expiresIn: 86400 
    });
    res.setHeader('x-access-token', 'Bearer '+ token);
    console.log("creation ok")
     userModel.findOne({username:req.body.username,password:req.body.pwd})
    .then(result=>{console.log(result);
     res.status(200).send({ auth: true, token: token ,id:result._id});
    })
     
}
}

catch (err) {
  console.log(err.message)
  res.send(err.message);
  }

} 

const verifyUser=function(list,name){

  for(let i=0;i<list.length;i++){
    if(list[i].username==name){
      return false
    }  
    }
    return true

}

  const findByUsername=async function(req, res){
    const usernameSaisi = req.params.username.toLowerCase();
    console.log(usernameSaisi)
      userModel.find({ username:usernameSaisi})
        .then(data => {
          if (!data)
            res.status(404).send({ message: "username couldn't be empty " + usernameSaisi });
          else {
            if (data.length == 0) {
              res.send("no user found")
            } else {
              res.send(data);
            }
          }
    
        })
        .catch(err => {
          res
            .status(500)
            .send(err.message);
        });
  }


exports.login = login;
exports.createUser=createUser;
exports.findByUsername=findByUsername;
