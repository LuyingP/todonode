const express = require("express")
const app=express();
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const db = require("./db/db.config")
const userService=require("./app/userService")
const cors = require("cors");
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.use(cookieParser())


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200");
   res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
  next();
});

  
app.post("/login", userService.login)
app.post("/register", userService.createUser)
app.get("/user/:username",userService.findByUsername)


db.connect()
  .then(() => {
    app.listen(3000,()=>{
      console.log(`server is running at 3000`);
  });
   
  });
  module.exports=app;